GOOGLE_PROJECT_ID=customer-roulette-325319

gcloud builds submit --tag gcr.io/$GOOGLE_PROJECT_ID/customer-roulette \
  --project=$GOOGLE_PROJECT_ID

gcloud beta run deploy customer-roulette \
  --image gcr.io/$GOOGLE_PROJECT_ID/customer-roulette \
  --platform managed \
  --region us-central1 \
  --project=$GOOGLE_PROJECT_ID
