const express = require('express');
const Firestore = require('@google-cloud/firestore');
const Joi = require("joi");
const _ = require("underscore");
const nodemailer = require('nodemailer');
const db = new Firestore();
const app = express();
app.use(express.json());
const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`customer-roulette is running on port ${port}`);
});

let transport = nodemailer.createTransport( {
    service: 'Gmail',
    auth: {
        user: 'badalmallik20@gmail.com',
        pass: 'Wifey@2139'
    }
});

app.post('/customer', async (req, res) => {
    try{
        let {firstName,lastName,email,gender}= req.body;
        const schema = Joi.object({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            gender: Joi.string().required().valid('male','female','others'),
            email: Joi.string().required().email(),
        })
        const validation = await schema.validate({firstName,lastName,email,gender});
        if(validation.error){
            throw 406
        }
        const query = db.collection('customers').where('email', '==', email.toLowerCase());
        const querySnapshot = await query.get();
        if (querySnapshot.size > 0) {
            throw 409;
        }

        const newCustomer = {
            firstName,
            lastName,
            email:(email).toLowerCase(),
            gender
        }
        await db.collection('customers').doc().set(newCustomer);
        res.status(201).json({ data: { customer: newCustomer } });
    }catch(error){
        console.log('error---',error)
        res.status(error).send('Error');
    }
    
})

app.get('/chooseCustomer', async () => {
    try{
        const query = await db.collection('customers');
        const querySnapshot = await query.get();
        if(querySnapshot.size>0) {
            const random= await _.random(0, (querySnapshot.size-1));
            if(querySnapshot.docs[random].data().email){
                let emailMessage = {
                    from: '"IKEA Team" <badalmallik20@gmail.com>',
                    to: querySnapshot.docs[random].data().email,
                    subject: 'Congrats',
                    text:'You are the choose one.'
                };
                transport.sendMail(emailMessage, function(error){
                    if(error){
                    console.log("error on sending mail ================",error);
                    }
                    return
                });
            }
        }
        return;
    }catch(error){
        console.log('error---',error)
        return
    }
    
})
 

 
