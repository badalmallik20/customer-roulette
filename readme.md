Customer Roulette

Introduction

This sample app does two things:

1. An event has been organised for customers. Their information is to be registered and managed with an application that uses an API.
2. At regular intervals during the day, a random customer will be selected to receive a message with a gift.

To deploy and try out the sample:

1. Create a Firebase project on the Firebase Console
2. Replace `GOOGLE_PROJECT_ID` in deploy.sh with your project id.
3. Install the required dependencies by running npm install in the functions directory.
4. Deploy your project's code running `./deploy.sh`.

Api Documentaion:-

    Create User:-

        Method- post
        path- `/customer`
        Request body-
            {
                "firstName":"FIRST_NAME_OF_CUSTOMER",
                "lastName":"LAST_NAME_OF_CUSTOMER",
                "gender":"GENDER_OF_CUSTOMER",
                "email":"EMAIL_OF_CUSTOMER"
            }
        
            Validation:-
                1. First Name is required.
                2. Last Name is required.
                3. Email is required and must in email format (e.g.username@domin.com).
                4. Gender is required and must have value from one of these `male`,`female` and `others`
            
            Response:- 
                Successful:
                    201: Created
                
                Error:
                    409: Conflict
                        Reason:- email address is already exists.

                    406: Not Acceptable
                        Reason:- request body is not acceptable. There is some valadition error.
                
    
